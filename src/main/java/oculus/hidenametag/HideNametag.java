package oculus.hidenametag;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.NameTagVisibility;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;
import org.bstats.bukkit.Metrics;

import java.util.Objects;

@SuppressWarnings("deprecation")
public class HideNametag extends JavaPlugin implements Listener {
    public static Scoreboard scoreboard;
    public static Team team;

    @Override
    public void onEnable() {
        new Metrics(this, 9984);

        saveDefaultConfig();
        reloadConfig();

        scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        team = scoreboard.registerNewTeam("hide_nametag");
        team.setNameTagVisibility(NameTagVisibility.NEVER);

        PluginManager pluginManager = getServer().getPluginManager();
        pluginManager.registerEvents(this, this);
        Bukkit.getOnlinePlayers().forEach(this::updateScoreboard);
    }

    @Override
    public void onDisable() {
        Bukkit.getOnlinePlayers().forEach(this::removeScoreboard);
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        updateScoreboard(event.getPlayer());
    }

    @EventHandler
    public void onPlayerChangedWorld(PlayerChangedWorldEvent event) {
        updateScoreboard(event.getPlayer());
    }

    public void removeScoreboard(Player player) {
        player.setScoreboard(Bukkit.getScoreboardManager().getMainScoreboard());
    }

    public void updateScoreboard(Player player) {
        for (String worldName : Objects.requireNonNull(getConfig().getStringList("EnabledWorlds"))) {
            if (worldName.equals(player.getWorld().getName())) {
                team.addEntry(player.getName());
                player.setScoreboard(scoreboard);
                return;
            }
        }

        if (team.hasEntry(player.getName())) {
            team.removeEntry(player.getName());
            removeScoreboard(player);
        }
    }
}