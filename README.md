# О Hide Nametag

Hide Nametag просто скрывает Nametag от всех игроков. \
Вы можете настроить все в файле **plugins/HideNametag/[config.yml](https://gitlab.com/Commandcracker/hide-nametag/-/blob/master/src/main/resources/config.yml)**.

## Требования

В настоящее время Hide Nametag поддерживает **CraftBukkit**, **Spigot** и [Paper](papermc.io/) **(рекомендуется)**. \
Другие реализации серверов могут работать, но мы не рекомендуем их, так как они могут вызвать проблемы с совместимостью.

## Ссылки

|Source|Artifacts|Stats|
|:---:|:---:|:---:|
|[Gitlab](https://gitlab.com/Commandcracker/hide-nametag)|[Spigot](https://www.spigotmc.org/resources/hide-nametag.89244)|[bStats](https://bstats.org/plugin/bukkit/Hide%20Nametag/9984)|
| |[Bukkit](https://dev.bukkit.org/projects/hide-nametag)|
| |[CurseForge](https://www.curseforge.com/minecraft/bukkit-plugins/hide-nametag)|